# Panorama exercise

An exercise in the course of Introduction to Image Processing. The whole project was mainly written in Python. 

## Authors

- Do Phu AN

- Luu Thanh Son

- Hoang Minh Quan

## Getting Started

This instruction will guide you how to set up development environment to test this application

### Prerequisites

This project needs the following libs and apps

```
- Python 3.x

- Opencv 2.x

- Opencv Contrib Lib

- Tkinter
```

### Setup

Install Python3 with Tkinter

```shell
brew install python3 --with-tcl-tk
brew postinstall python3
```

Install Opencv
```shell
brew install opencv
```

or install Opencv 3 with contrib lib

```shell
brew install opencv3 --with-contrib --with-python3
```

Install other dependencies

```shell
pip install numpy scipy scikit-image matplotlib scikit-learn
```

### Features

This application will stitch 2 images to make a panorama photos

### Function Description

In this project, features and keypoints from 2 images are extracted by using SURF features detector library of Opencv. There is a difference between 2 version of OpenCV as listed below:

- OpenCV 2.x:

This version of Opencv still support SIFT and SURF features detection as a built in function. Therefore, the detector can be called directly as the code below:

```python
detector = cv2.FeatureDetector_create("SURF")
kps = detector.detect(gray)

extractor = cv2.DescriptorExtractor_create("SURF")
(kps, features) = extractor.compute(gray, kps)
```

- Opencv 3.x:

This version of Opencv no longer supports built-in feature detectors such as SIFT and SURF. If developers want to implement feature extraction function, they must install additional library or install OpenCV contrib lib as aforementioned. 

### License
[![License](https://img.shields.io/:license-gnu-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
